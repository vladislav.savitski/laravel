@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Question
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                @include('common.errors')

                <!-- New Task Form -->
                    <form action="{{ url('question-add')}}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    <!-- Task Name -->
                        <div class="form-group">
                            <label for="question-name" class="col-sm-3 control-label">Question name</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" id="question-name" class="form-control" value="{{ old('question') }}">
                            </div>

                            <br>

                            <label for="question-text" class="col-sm-3 control-label">Question text</label>
                            <div class="col-sm-6">
                                <input type="text" name="text" id="question-text" class="form-control" value="{{ old('question') }}">
                            </div>

                            <br>

                            <label for="question-type" class="col-sm-3 control-label">Question type</label>
                            <div class="col-sm-6">
                                <input type="number" name="type" id="question-type" class="form-control" value="{{ old('question') }}">
                            </div>
                            <br>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Question
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <br>
            @if(count($questions) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Questions
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped question-table">
                            <thead>
                            <th>Questions</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @foreach ($questions as $question)
                                <tr>
                                    <td class="table-text"><div>{{ $question->name }}</div></td>

                                    <!-- Task Delete Button -->
                                    <td>
                                        <form action="{{ url('question/'.$question->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif


        </div>
    </div>
@endsection