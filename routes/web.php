<?php
use Illuminate\Http\Request;
use App\Question;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


Route::get('/question', function () {
     return view('question', [
        'questions' => Question::orderBy('created_at', 'asc')->get()
    ]);
});

Route::post('/question-add', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'text' => 'required|max:255',
        'type' => 'required'
    ]);

    if ($validator->fails()) {
        return redirect('/question')
            ->withInput()
            ->withErrors($validator);
    }

    $question = new Question;
    $question->name = $request->name;
    $question->text = $request->text;
    $question->type = $request->type;
    $question->save();
    return redirect('/question');
});

Route::delete('/question/{id}', function ($id) {
    Question::findOrFail($id)->delete();
    return redirect('/question');
});